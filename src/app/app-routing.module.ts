import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {TermsAndConditionComponent} from './terms-and-condition/terms-and-condition.component';
import { MetaGuard } from '@ngx-meta/core';

const routes: Routes = [
  {
    path: '',
    component : HomeComponent,
    pathMatch : 'full'
  },
  {
    path: 'privacy-policy',
    component : PrivacyPolicyComponent,
    pathMatch : 'full'
  },
  {
    path: 'terms-and-condition',
    component : TermsAndConditionComponent,
    data: {
      meta: {
        title: 'Sweet home',
        description: 'Home, home sweet home... and what?'
      }
    },
    pathMatch : 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
